module gitee.com/jmdvirus/jkgo

go 1.15

require (
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golangers/log v0.0.0-20130127145259-9d0784362cb6
	github.com/kardianos/service v1.2.0
	github.com/mattn/go-sqlite3 v1.14.10
	github.com/onsi/gomega v1.14.0 // indirect
	github.com/tyranron/daemonigo v0.3.1
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
