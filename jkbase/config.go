package jkbase

import (
	"encoding/json"
	"io/ioutil"
)

type ConfigBaseArgs struct {
	Addr       string
	Port       int
	LogFile    string
	LogSize    int
	ClientAddr string
	ClientPort int
}

func GetConfigInfo(filepath string, ci interface{}) error {
	d, e := ioutil.ReadFile(filepath)
	if e != nil {
		return e
	}
	err := json.Unmarshal(d, ci)
	if e != nil {
		return err
	}
	return nil
}

func CMConfigFile(filepath string, ci interface{}) error {
	return GetConfigInfo(filepath, ci)
}
