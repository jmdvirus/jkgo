// +build linux

package jkbase

// jdaemon "github.com/tyranron/daemonigo"

func InitDeamon(backrun bool) {
	if backrun {
		// Daemonizing echo server application.
		/*
			switch isDaemon, err := jdaemon.Daemonize("start"); {
			case !isDaemon:
				return
			case err != nil:
				jklog.L().Errorln("daemon start failed : ", err.Error())
			}
		*/
	}
}

func InitProgramRun(cmd, desc string, start func()) (*Program, error) {

	prog := NewProgram(cmd, cmd, desc)
	docmd := "run"

	prog.Runner = start
	err := prog.CreateService()
	if err == nil {
		err = prog.Ctrl(docmd)
		return nil, err
	}
	return prog, nil
}
