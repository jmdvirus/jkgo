package jkbase

// web interface for easy to use in some special only need simple page
// example in demo/jkbase_test

import (
	"encoding/json"
	"errors"
	"jkgo/jk/jklog"
	"jkgo/jk/utils"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
)

type WebBaseHandle struct {
	path      string
	csspath   string
	jspath    string
	imgpath   string
	respath   string
	addpath   string
	port      int
	templates []string
}

type WebBaseInfo struct {
	child   interface{}
	Handler *WebBaseHandle
}

func (h *WebBaseHandle) BasePath() string {
	return h.path
}

func NewWebBaseHandle(port int, path string) (*WebBaseHandle, error) {
	dir, err := os.Getwd()
	if err != nil {
		jklog.L().Errorln("get current error ", err)
	}

	bpath := os.Getenv("BASEPATH")
	if len(bpath) > 0 {
		dir = bpath
	}
	path = dir + "/" + path

	wbh := &WebBaseHandle{
		port:    port,
		path:    path,
		csspath: path,
		jspath:  path,
		imgpath: path,
		respath: path,
		addpath: path,
	}
	jklog.L().Debugf("Use addon path is [%s], css path [%s]\n",
		wbh.addpath, wbh.csspath)
	http.Handle("/css/", http.FileServer(http.Dir(wbh.csspath)))
	http.Handle("/js/", http.FileServer(http.Dir(wbh.jspath)))
	http.Handle("/imgs/", http.FileServer(http.Dir(wbh.imgpath)))
	http.Handle("/res/", http.FileServer(http.Dir(wbh.respath)))
	http.Handle("/addon/", http.FileServer(http.Dir(wbh.addpath)))

	s := wbh.getTemplates(path + "/template")
	if s != nil {
		wbh.templates = s
		jklog.L().Debugln("template: ", wbh.templates)
	}
	return wbh, nil
}

func (w *WebBaseHandle) getTemplates(dir string) []string {
	s := []string{}
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			jklog.L().Errorln(err)
			return err
		}
		if info.IsDir() {
			return nil
		}
		if strings.LastIndex(path, ".html") > 0 {
			s = append(s, path)
		}
		return nil
	})
	return s
}

// Will Block
func (wbh *WebBaseHandle) Listen() error {
	jklog.L().Infoln("start listen with :", wbh.port)
	err := http.ListenAndServe(":"+strconv.Itoa(wbh.port), nil)
	if err != nil {
		return err
	}
	return errors.New("listen exit")
}

func (b *WebBaseInfo) SetFunc(url string, child interface{}) {
	b.child = child
	http.HandleFunc(url, b.serverHttp)
}

func (b *WebBaseInfo) Parses(w http.ResponseWriter, data interface{}, filename ...string) error {
	sp := SimpleParse{}
	files := []string{}
	for _, v := range filename {
		files = append(files, v)
	}
	for _, v := range b.Handler.templates {
		files = append(files, v)
	}
	err := sp.Parses(w, data, files...)
	return err
}

// deprecated
func (b *WebBaseInfo) Parse(w http.ResponseWriter, filename string, data interface{}) error {
	sp := SimpleParse{}
	err := sp.Parse(w, filename, data)
	return err
}

func (b *WebBaseInfo) GenerateResponse(w http.ResponseWriter, data interface{}, status int) {
	res := utils.M{
		"Status": status,
	}
	res["Result"] = data
	d, _ := json.Marshal(res)
	w.Write(d)
}

func (b *WebBaseInfo) serverHttp(w http.ResponseWriter, r *http.Request) {
	c := reflect.ValueOf(b.child)
	inputs := make([]reflect.Value, 2)
	inputs[0] = reflect.ValueOf(w)
	inputs[1] = reflect.ValueOf(r)
	jklog.L().Debugf("URL: method: %s, %s, %s, %s\n", r.Method, r.URL.String(), r.RemoteAddr,
		r.UserAgent())

	switch r.Method {
	case "GET":
		method := c.MethodByName("Get")
		if method.IsValid() {
			method.Call(inputs)
		} else {
			jklog.L().Warnln("Undefined GET")
		}
		break
	case "POST":
		method := c.MethodByName("Post")
		if method.IsValid() {
			method.Call(inputs)
		} else {
			jklog.L().Warnln("Undefined POST")
		}
		break

	}
}
