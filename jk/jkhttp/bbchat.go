package jkhttp

import "encoding/json"

type BBChatAttachInfo struct {
	Text  string `json:"text"`
	Color string `json:"color"`
}

type BBChatInfo struct {
	Text   string             `json:"text"`
	Attach []BBChatAttachInfo `json:"attachments"`
}

type BBChat struct {
	hQuery *JKHttpQuery
	data   BBChatInfo
}

func NewBBChat(title string, data string) (*BBChat, error) {
	x := &BBChat{}
	att := BBChatAttachInfo{
		Text:  data,
		Color: "ffa50b",
	}
	x.data.Attach = append(x.data.Attach, att)
	x.data.Text = title
	return x, nil
}

func (p *BBChat) Post() ([]byte, error) {
	url := "https://hook.bearychat.com/=bwAwS/incoming/403adb11ad853515da1d0b074414cf3d"
	r, err := json.Marshal(p.data)
	if err != nil {
		return nil, err
	}
	p.hQuery, _ = NewJKHttpQuery(url, string(r))
	return p.hQuery.Post()
}
