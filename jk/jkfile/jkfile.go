package jkfile

import (
	"io/ioutil"
)

func JKGetAllFile(pathname string) ([]string, error) {
	var d = []string{}
	rd, err := ioutil.ReadDir(pathname)
	for _, fi := range rd {
		if fi.IsDir() {
			nextPath := pathname + "/" + fi.Name()
			dout, _ := JKGetAllFile(nextPath)
			for _, v := range dout {
				vv := fi.Name() + "/" + v
				d = append(d, vv)
			}
		} else {
			d = append(d, fi.Name())
		}
	}
	return d, err
}
