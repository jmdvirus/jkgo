package helper

import "testing"

func TestTimeConvertBenewServer(t *testing.T) {
	origin := "2019-09-11T20:06:07.717Z"
	tt, ms := TimeConvertBenewServer(origin)
	if tt != "2019-09-11 20:06:07" || ms != 717 {
		t.Fatal(t, ",", ms)
	}
}
