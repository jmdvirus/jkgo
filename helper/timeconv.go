package helper

import (
	"errors"
	"strconv"

	//"fmt"
	"strings"
	"time"
)

const (
	JKFormatString         = "2006-01-02-15:04:05"
	jkformatStringFile     = "2006-01-02-15-04-05"
	jkformatStringFileDate = "2006-01-02"
	jkformatLogDevice      = "02/01/2006 15:04:05"
	JKFormatNormalNoSec    = "2006-01-02 15:04"
)

var Month = map[string]string{
	"Jan": "01",
	"Feb": "02",
	"Mar": "03",
	"Apr": "04",
	"May": "05",
	"Jun": "06",
	"Jul": "07",
	"Aug": "08",
	"Sep": "09",
	"Oct": "10",
	"Nov": "11",
	"Dec": "12",
}

//convert from "Fri Aug 10 09:40:40 +0800 2012" to "2012.8.10 09:40"
func TimeConv(origin string) (after string) {
	arr1 := strings.Split(origin, " ")
	//Fri Aug 10 09:40:40 +0800 2012
	// 0   1   2     3      4     5
	arr2 := strings.Split(arr1[3], ":")

	after = arr1[5] + "." +
		Month[arr1[1]] + "." +
		arr1[2] + " " +
		arr2[0] + ":" +
		arr2[1]

	return
}

// 2019-09-11T20:06:07.717Z
func TimeConvertBenewServer(origin string) (after string, ms int) {
	after = origin[:10] + "-" + origin[11:19]
	ms, _ = strconv.Atoi(origin[20:23])
	return
}

func LogDeviceTimeConv(origin string) (after string) {
	arr1 := strings.Split(origin, " ")
	if len(arr1) == 5 {
		// Aug  6 2019 09:40:50
		after = arr1[2] + "/" + Month[arr1[0]] + "/" + arr1[3] + " " +
			arr1[4]
	} else if len(arr1) == 4 {
		// Aug 26 2019 09:40:50
		after = arr1[1] + "/" + Month[arr1[0]] + "/" + arr1[2] + " " +
			arr1[3]
	} else {
		// Aug 26 2019:09:40:50
		arr2 := strings.Split(arr1[2], ":")
		after = arr1[1] + "/" + Month[arr1[0]] + "/" + arr2[0] +
			" " + arr2[1] + ":" + arr2[2] + ":" + arr2[3]
	}
	return after
}

func Time1972TimeStamp() int64 {
	t, _ := ConvertStringToUnixTime("1970-12-31-23:59:59")
	return t
}

func ConvertStringToUnixTime(tm string) (int64, error) {
	t, err := time.Parse(JKFormatString, tm)
	if err != nil {
		return 0, err
	} else {
		return t.Unix(), nil
	}
}

func ConvertStringToUnixTimeFormat1(tm string) (int64, error) {
	t, err := time.ParseInLocation(jkformatStringFile, tm, time.Local)
	if err != nil {
		return 0, err
	} else {
		return t.Unix(), nil
	}
}

func FormUnixTimeToString(t int64) string {
	tm := time.Unix(t, 0)
	return tm.Format(jkformatStringFile)
}

func FormUnixTimeToStringDate(t int64) string {
	tm := time.Unix(t, 0)
	return tm.Format(jkformatStringFileDate)
}

func UnxiToFormatString(t int64, format string) string {
	tm := time.Unix(t, 0)
	return tm.Format(format)
}

func FormatStringToUnix(tm, format string) (int64, error) {
	loc, _ := time.LoadLocation("Local")
	t, err := time.ParseInLocation(format, tm, loc)
	if err != nil {
		return 0, err
	} else {
		return t.Unix(), nil
	}
}

func FormatStringToUnixLogDevice(tm string) (int64, error) {
	return FormatStringToUnix(tm, jkformatLogDevice)
}

func LogDeviceUnixTimeToStringNormal(t int64) string {
	if t < 0 {
		return "InvalidTime"
	}
	tm := time.Unix(t, 0)
	return tm.Format(jkformatLogDevice)
}

// xxxx.ms
func LogDeviceFormatStringToUnixMS(tm string) (int64, error) {
	tt := strings.Split(tm, ".")
	if len(tt) != 2 {
		return -1, errors.New("Invalid time string")
	}
	i, err := FormatStringToUnixLogDevice(tt[0])
	if err != nil {
		return -1, err
	}
	ms, _ := strconv.Atoi(tt[1])
	return i*1000 + int64(ms), nil
}

// t contains ms (3 numbers)
func LogDeviceUnixTimeToString(t int64) string {
	if t < 0 {
		return "InvalidTime"
	}
	t1 := t / 1000
	t2 := t % 1000
	tm := time.Unix(t1, 0)
	return tm.Format(jkformatLogDevice) + "." + strconv.Itoa(int(t2))
}
