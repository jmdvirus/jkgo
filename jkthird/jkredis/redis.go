package jkredis

import (
	"context"
	"github.com/go-redis/redis"
	"time"
)

var ctx = context.Background()

type ThirdRedisHandler struct {
	url    string
	user   string
	pass   string
	client *redis.Client
}

func ThirdRedisHandlerCreate(url string, user, pass string) (*ThirdRedisHandler, error) {
	trh := &ThirdRedisHandler{
		url:  url,
		user: user,
		pass: pass,
	}
	trh.client = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "jmdredis123",
		DB:       0,
	})

	st := trh.client.Ping()
	if st.Err() != nil {
		trh.client.Close()
		return nil, st.Err()
	}

	return trh, nil
}

func (t *ThirdRedisHandler) Close() {
	t.client.Close()
}

func (t *ThirdRedisHandler) Set(key string, value interface{}) error {
	st := t.client.Set(key, value, time.Duration(time.Hour*24))
	return st.Err()
}

func (t *ThirdRedisHandler) Get(key string, value interface{}) error {
	st := t.client.Get(key)
	value = st.Val()
	return st.Err()
}
