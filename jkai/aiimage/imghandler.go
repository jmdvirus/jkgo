package aiimage

import (
	"io/ioutil"
	"jkgo/jkbase"
	"os"
)

type ImgHandler struct {
	imgdata []byte
}

func NewImgHandler(imgdata []byte) *ImgHandler {
	return &ImgHandler{
		imgdata: imgdata,
	}
}

func (ih *ImgHandler) Analyse() string {
	return ""
}

func (ih *ImgHandler) SaveFile(filename string) error {
	b, err := jkbase.Base64Decode(string(ih.imgdata))
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filename, b, os.ModePerm)
}
