jkgo
====
独立开放功能全集

目录结构
```
  - helper 一些通用小功能集，将逐步整体到 jkbase 中
  - jkbase 基础功能集
  - jkdbs 基础数据库操作功能库
  - jk 带有业务功能的接口集，其中部分业务的将逐步移入 jkbase
  - jkmisc 其它基础 
  - jkws Websocket 
  - jkai AI functions 

基础功能模块，基于 go mod 
```
