package jktime

import (
	"testing"
	"time"
)

func TestTimeCompareOne(t *testing.T) {
	toComp := "[{\"startTime\":{\"year\":2019,\"month\":9,\"day\":4," +
		"\"hour\":16,\"minute\":30,\"second\":5}," +
		"\"endTime\":{\"year\":2019,\"month\":9,\"day\":4," +
		"\"hour\":16,\"minute\":30,\"second\":5}," +
		"\"week\":[3]" +
		"}]"
	h, err := NewTimeCompare(toComp)
	if err != nil {
		t.Fatal(err)
	}
	now := time.Date(2019, 9, 4, 16, 30, 5, 0, time.Local)
	ret := h.InTime(now)
	if !ret {
		t.Fatal("should true")
	}
}

func TestTimeCompareIn(t *testing.T) {
	toComp := "[" +
		"{\"startTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"endTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"week\":[3]}" +
		",{\"startTime\":{\"year\":2019,\"month\":7,\"day\":4," +
		"\"hour\":16,\"minute\":10,\"second\":5}," +
		"\"endTime\":{\"year\":2019,\"month\":10,\"day\":4," +
		"\"hour\":16,\"minute\":30,\"second\":5}," +
		"\"week\":[3]}" +
		"]"
	h, err := NewTimeCompare(toComp)
	if err != nil {
		t.Fatal(err)
	}
	now := time.Date(2019, 9, 4, 16, 30, 5, 0, time.Local)
	ret := h.InTime(now)
	if !ret {
		t.Fatal("should true")
	}
}

func TestTimeCompareNoSecond(t *testing.T) {
	toComp := "[" +
		"{\"startTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"endTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"week\":[3]}" +
		",{\"startTime\":{\"year\":2019,\"month\":7,\"day\":4," +
		"\"hour\":16,\"minute\":10,\"second\":-1}," +
		"\"endTime\":{\"year\":2019,\"month\":10,\"day\":4," +
		"\"hour\":16,\"minute\":31,\"second\":-1}," +
		"\"week\":[3]}" +
		"]"
	h, err := NewTimeCompare(toComp)
	if err != nil {
		t.Fatal(err)
	}
	now := time.Date(2019, 9, 4, 16, 30, 5, 0, time.Local)
	ret := h.InTime(now)
	if !ret {
		t.Fatal("should true")
	}
}

func TestTimeCompareNoDate(t *testing.T) {
	toComp := "[" +
		"{\"startTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"endTime\":{\"year\":2019,\"month\":2,\"day\":4," +
		"\"hour\":16,\"minute\":33,\"second\":5}," +
		"\"week\":[3]}" +
		",{\"startTime\":{\"year\":-1,\"month\":7,\"day\":-1," +
		"\"hour\":16,\"minute\":10,\"second\":5}," +
		"\"endTime\":{\"year\":-1,\"month\":10,\"day\":-1," +
		"\"hour\":16,\"minute\":31,\"second\":10}," +
		"\"week\":[3]}" +
		"]"
	h, err := NewTimeCompare(toComp)
	if err != nil {
		t.Fatal(err)
	}
	now := time.Date(2019, 9, 4, 16, 30, 5, 0, time.Local)
	ret := h.InTime(now)
	if !ret {
		t.Fatal("should true")
	}
}
