package data

import (
	"jkgo/helper"
	"testing"
)

func TestNewCmdBase(t *testing.T) {
	x, _ := NewCmdBase()
	m := helper.NewMongo("localhost/project")
	attr := map[string]string{
		"test1": "value1",
		"test2": "value2",
	}
	err := x.CmdSimple(m, 0, "test", "testresult", "noting", attr)
	if err != nil {
		t.Fatal(err)
	}
}
