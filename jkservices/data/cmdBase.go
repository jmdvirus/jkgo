package data

import (
	"encoding/json"
	"jkgo/helper"
)

type JKCmdBaseInfo struct {
	CreateTime int64  `bson:"createTime"`
	RecordTime int64  `bson:"recordTime"`
	UpdateTime int64  `bson:"updateTime"`
	Id         int64  `bson:"id"`
	Cmd        string `bson:"cmd"`
	SubCmd     string `bson:"subCmd"`
	Title      string `bson:"title"`
	Content    string `bson:"content"`
	Source     string `bson:"source"`
	Attr       string `bson:"attr"`
}

type JKCmdBase struct {
	baseInfo JKCmdBaseInfo
}

func NewCmdBase() (*JKCmdBase, error) {
	return &JKCmdBase{}, nil
}

func (c *JKCmdBase) AddAttr(a *map[string]string) error {
	data, err := json.Marshal(*a)
	if err != nil {
		return err
	}
	c.baseInfo.Attr = string(data)
	return nil
}

func (c *JKCmdBase) SaveToDb(db *helper.Mongo, col string) error {
	m := db.Cnew("project", col)
	return m.Insert(c.baseInfo)
}
