package data

import (
	"jkgo/helper"
	"time"
)

func (c *JKCmdBase) CmdSimple(db *helper.Mongo, id int64, cmd, subcmd, content string, attr map[string]string) error {
	c.baseInfo.Id = id
	c.baseInfo.Cmd = cmd
	c.baseInfo.SubCmd = subcmd
	c.baseInfo.Content = content
	c.baseInfo.CreateTime = time.Now().Unix()
	c.baseInfo.UpdateTime = c.baseInfo.CreateTime
	c.baseInfo.RecordTime = c.baseInfo.CreateTime
	err := c.AddAttr(&attr)
	if err != nil {
		return err
	}
	return c.SaveToDb(db, "cmdBase")
}
