package dbRead

type DBTableInfo struct {
	KeyMap map[string]string
}

type DBReadInterface interface {
	Connect(host, user, pass string) error
	Read(db, table string) (DBTableInfo, error)
	Close()
}
