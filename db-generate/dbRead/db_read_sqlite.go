package dbRead

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

type DBReadSqlite struct {
	db *sql.DB
}

func (s *DBReadSqlite) Connect(host, user, pass string) error {
	var err error
	s.db, err = sql.Open("sqlite3", host)
	if err != nil {
		return err
	}
	return nil
}

func (s *DBReadSqlite) Read(db, table string) (DBTableInfo, error) {
	queryString := "select * from " + table
	rows, err := s.db.Query(queryString)
	if err != nil {
		return DBTableInfo{}, err
	}
	tableInfo := DBTableInfo{}
	tableInfo.KeyMap = make(map[string]string, 10)
	v, _ := rows.ColumnTypes()
	for _, x := range v {
		n := x.Name()
		t := x.DatabaseTypeName()
		tableInfo.KeyMap[n] = t
	}
	return tableInfo, nil
}

func (s *DBReadSqlite) Close() {
	s.db.Close()
}
