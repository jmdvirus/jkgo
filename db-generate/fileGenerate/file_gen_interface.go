package fileGenerate

import "dbGenerate/dbRead"

type FileGenProperty struct {
	OutPos  string // 生成到哪里
	Prefix  string // 生成文件名和函数名前缀
	Table   string // 文件名和函数名均会包括表名
	SqlType string // 数据库类型，文件名和函数名会有
}

type FileGenInterface interface {
	GenerateHeader(info dbRead.DBTableInfo, property FileGenProperty) error
	GenerateImpl(info dbRead.DBTableInfo, property FileGenProperty) error
}
