package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"jkgo/jk/jklog"
	"jkgo/jk/utils"
	"jkgo/jkbase"
	"jkgo/jkws"
)

var (
	addr = flag.String("addr", "0.0.0.0", "websocket address")
	port = flag.Int("port", 8090, "websocket port")
	sec  = flag.Bool("sec", false, "websocket with secure")
	url  = flag.String("url", "jkws", "websocket base url")
)

func main() {
	flag.Parse()

	jklog.L().Infoln("jkws client test program start")

	ws, err := jkws.NewWSHandlerClient(*addr, *port, *sec, *url)
	if err != nil {
		jklog.L().Errorln(err)
		return
	}
	if *sec {
		err = ws.ConnectTls(3)
	} else {
		err = ws.Connect(3)
	}
	if err != nil {
		jklog.L().Errorln(err)
		return
	}

	xmsg := servicebase.MsgPayload{}
	xmsg.Make("jkwsclient", "imgdetect", 5, "plant", "imagedata")

	img, err := ioutil.ReadFile("/opt/data/testav/1.jpg")
	if err != nil {
		jklog.L().Errorln(err)
		return
	}
	baseimg := jkbase.Base64Encode(img)
	xmsg.Data = baseimg
	xdata, err := json.Marshal(xmsg)
	if err != nil {
		jklog.L().Errorln(err)
		return
	}
	len := len(xdata)

	nxdata := utils.Int32ToBytes(int32(len))

	jklog.L().Infoln("send data length = ", len, " - ", nxdata)
	ws.Send(nxdata, false)
	ws.Send(xdata, true)

	jklog.L().Infoln("jkws client exit...")
}
